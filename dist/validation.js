/**
 * Wadachi FuelPHP Validation Client Package
 *
 * Automatic validation error display for the browser.
 *
 * @package    wi-validation-client
 * @version    1.0.0
 * @author     Wadachi, Inc.
 * @link       http://wdch.jp
 */

window.Validation = (function($, JSON) {
  "use strict";

  // 検証エラーをフラグする
  function flagError(input, message) {
    var parent;

    notify("body", "wi.willFlagError", input[0]);

    input = $(input);
    parent = input.parent();
    parent.addClass("has-error");
    parent.append("<div class='error'>" + message + "</div>");

    notify("body", "wi.didFlagError", input[0]);
  }

  // ページの入力要素を検索する
  function findInput(name) {
    var input, label, options;

    input = $(":input[name='" + name + "']");
    options = input.filter(":checkbox, :radio");

    // チェックボックスとラジオボタンのラベルの下に検証エラーが表示される
    if (options.length) {
      input = options.filter(":checked:first").length ? options.filter(":checked:first") : options.filter(":first");

      if (input.parent("label").length) {
        label = input.parent("label");  // ラベルは入力を包み込む
      } else {
        label = $("label[for=" + input.prop("id") + "]");  // ラベルは入力の隣にいる
      }

      input = label.length ? label : input;
    }

    return input;
  }

  // 組み込んだデータを取得する
  function getEmbeddedData(selector) {
    var json;

    if (!JSON)
      return [];

    selector = $(selector);
    json = selector.html() || "{}";

    return JSON.parse(json);
  };

  // イベントを発射する
  function notify(source, evt, data) {
    try {
      $(source).trigger(evt, data);
    } catch(e) {
      // 何もしない
    }
  }

  // 要素にスクロールする
  function scrollTo(element) {
    element = $(element);

    if (element.is(":visible")) {
      $("html, body").animate({
        scrollTop: element.offset().top - 150
      }, 2000);
    }
  }

  // 検証メッセージを自動表示する
  $(function() {
    var errors, firstErrorOnPage, input, name;

    errors = getEmbeddedData("#input-errors");

    // 検証メッセージを表示する
    for (name in errors) {
      input = findInput(name);
      firstErrorOnPage = firstErrorOnPage || input;
      flagError(input, errors[name]);
    }

    // 最初の検証エラーにスクロールする
    scrollTo(firstErrorOnPage);
  });
})(window.jQuery, window.JSON);
